const path = require('path');
const fs = require('fs');

const configFilePath = path.join(process.cwd(), '.yenmoc.json');

if (fs.existsSync(configFilePath)) {
  const data = fs.readFileSync(configFilePath);
  module.exports = JSON.parse(data);
} else {
  module.exports = {};
}
